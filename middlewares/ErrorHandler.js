module.exports = errorHandler = (err, req, res, next) => {
  if (err) {
    if (err.status) {
      res.status(err.status).json({
        status: 'failed',
        message: err.message
      })
    } else if (err.response && err.response.data) {
      res.status(400).json({
        status: 'failed',
        message: err.response.data.error
      })
    } else {
      res.status(422).json({
        status: 'failed',
        message: `${err.name}: ${err.message}`
      })
    }
  }
}