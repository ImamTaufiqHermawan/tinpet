const models = require('../models');
const {
    Profile
} = require('../models');

module.exports = modelsName => {
    const model = models[modelsName];
    return async function (req, res, next) {
        try {
            let user = await model.findByPk(req.params.id, {
                include: [{
                    model: Profile
                }]
            });
            if (!user) throw new Error("user doesn't exist");
            req.dataUser = user
            // console.log(req.dataUser);
            // console.log(req.dataUser.Profile.id, '==============data profile========');
            // console.log(req.dataUser.id, '=================id user=========');
            // res.end()
            next()
        } catch (err) {
            res.status(400).json({
                status: 'failed',
                message: [err.message]
            })
        }
    }
}