const models = require('../models');
module.exports = (modelName) => {
    const Model = models[modelName]

    return async (req, res, next) => {
        try {
            const findUserId = await Model.findByPk(req.params.id)
            if (findUserId.UserId !== req.user.id) {
                throw new Error("You don't have approval to do this!")
            } else {
                next();
            }
        } catch (err) {
            res.status(400).json({
                status: 'failed',
                message: [err.message]
            })
        }
    }
}