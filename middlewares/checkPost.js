const models = require('../models');
const {
    Profile,
    User
} = require('../models');

module.exports = modelsName => {
    const model = models[modelsName];
    return async function (req, res, next) {
        try {
            let post = await model.findByPk(req.params.id, {
                include: [{
                    model: Profile
                }, {
                    model: User
                }]
            });
            if (!post) throw new Error("post doesn't exist");
            req.post = post;
            // console.log(req.post);
            // console.log(req.post.id, '=====pet id======');
            // console.log(req.post.User.email, '========ini email dari profile=======');
            // console.log(req.post.User.id, '===========user id==================');
            // console.log(req.post.Profile.id, '==============profile id================');
            // console.log(req.post.Profile.mobile_number, '===========mobile number===========');

            // res.end()
            next()
        } catch (err) {
            res.status(400).json({
                status: 'failed',
                message: [err.message]
            })
        }
    }
}