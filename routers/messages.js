const router = require('express').Router();
const Messages = require('../controllers/messages');
const Auth = require('../middlewares/authentication');
const CheckOwner = require('../middlewares/checkMessage');
const CheckUser = require('../middlewares/checkUser');

router.post('/:id', Auth, CheckUser('User'), Messages.create)
router.delete('/:id', Auth, Messages.delete)
router.get('/all', Auth, Messages.findAll)
router.put('/:id', Auth, CheckOwner('Messages'), Messages.update)
router.get('/count', Auth, Messages.findAllAndCountAll)

module.exports = router