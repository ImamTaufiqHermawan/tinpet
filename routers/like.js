const router = require('express').Router()
const Like = require('../controllers/like');
const Auth = require('../middlewares/authentication');
const checkPost = require('../middlewares/checkPost');

router.post('/likes/:id', Auth, checkPost('Pet'), Like.like);

module.exports = router