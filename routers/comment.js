const router = require('express').Router()
const commentController = require('../controllers/commentController');
const auth = require('../middlewares/authentication');
const checkPost = require('../middlewares/checkPost');

router.post('/:id', auth, checkPost('Pet'), commentController.comment);
router.get('/:id', auth, checkPost('Pet'), commentController.getComment);
router.delete('/:id', auth, commentController.deleteComment);

module.exports = router