const router = require('express').Router();
const User = require('./user');
const Pet = require('./Pet');
const Search = require('./search');
const Request = require('./Request');
const Documentation = require('./documentation');
const Comment = require('./comment');
const Like = require('./like');
const Message = require('./messages');
const Notification = require('./notification');

router.use('/api/v1/requests', Request)
router.use('/api/v1', Documentation);
router.use('/api/v1/users', User);
router.use('/api/v1/pets', Pet);
router.use('/api/v1/searches', Search);
router.use('/api/v1/comments', Comment);
router.use('/api/v1', Like);
router.use('/api/v1/messages', Message)
router.use('/api/v1/notifications', Notification);

module.exports = router