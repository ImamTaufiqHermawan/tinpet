const router = require('express').Router();
const Pet = require('../controllers/Pet');
const Auth = require('../middlewares/authentication');
const CheckOwner = require('../middlewares/checkCredentials');
const upload = require('../middlewares/uploader');

router.post('/', Auth, upload.single('image'), Pet.create)
router.put('/:id', Auth, CheckOwner('Pet'), upload.single('image'), Pet.update)
router.delete('/:id', Auth, CheckOwner('Pet'), Pet.delete)
router.get('/all', Auth, Pet.getAll)
router.get('/', Auth, Pet.getMyPost)

module.exports = router