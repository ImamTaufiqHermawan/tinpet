const router = require('express').Router();
const Auth = require('../middlewares/authentication');
const Search = require('../controllers/search');

router.get('/', Auth, Search.searchByCategoryAndLocation)

module.exports = router