const router = require('express').Router();
const Auth = require('../middlewares/authentication');
const Request = require('../controllers/Request');
const checkPost = require('../middlewares/checkPost');

router.post('/:id', Auth, checkPost('Pet'), Request.sendRequest)
router.delete('/', Auth, Request.delete)
router.get('/', Request.getAllRequest)

//! Approved or Rejected for update status Pets
router.put('/approved/:id', Auth, Request.approved)
router.put('/rejected/:id', Auth, Request.rejected)

module.exports = router