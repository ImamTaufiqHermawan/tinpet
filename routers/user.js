const router = require('express').Router()
const userController = require('../controllers/userController')
const profileController = require('../controllers/profileController')
const auth = require('../middlewares/authentication')
const Ownership = require('../middlewares/checkCredentials')
const upload = require('../middlewares/uploader');

router.post('/register', userController.register);
router.post('/login', userController.login);
router.put('/profile', auth, upload.single('image'), profileController.updateProfile);
router.get('/', auth, userController.userData);

module.exports = router