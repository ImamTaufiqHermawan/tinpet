const router = require('express').Router()
const Notification = require('../controllers/notification');
const Auth = require('../middlewares/authentication');

router.get('/', Auth, Notification.getNotifications);
router.get('/count', Auth, Notification.notificationCount);
router.delete('/:id', Auth, Notification.delete)

module.exports = router