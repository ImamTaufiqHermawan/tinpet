const router = require('express').Router()

//! Setup swagger documentation
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('../swagger.json')

//! Swagger implementation
router.use('/documentation', swaggerUi.serve) //! initial for swegger UI express, this is especially for the UI swegger setup
router.get('/documentation', swaggerUi.setup(swaggerDocument))

module.exports = router