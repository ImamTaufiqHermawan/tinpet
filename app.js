require('dotenv').config();
const express = require('express');
const app = express()
const logger = require('morgan');
const cors = require('cors');
const router = require('./routers');
const errorHandler = require('./middlewares/ErrorHandler');

app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))

if (process.env.NODE_ENV !== 'test')
    app.use(logger('dev'))
app.use(cors())
app.options('*', cors());

app.use(router)

app.get('/', (req, res) => {
    res.status(200).send('<h1>Welcome To Our Tinpet</h1>')
})

app.use(errorHandler)

module.exports = app