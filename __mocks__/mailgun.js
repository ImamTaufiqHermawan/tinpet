const Mailgun = jest.genMockFromModule('mailgun-js');
const faker = require('faker');

Mailgun.prototype.send = () => {
    return Promise.resolve({
        email: faker.internet.email()
    })
}

Mailgun.prototype.messages = function () {
    return this
}

module.exports = Mailgun;