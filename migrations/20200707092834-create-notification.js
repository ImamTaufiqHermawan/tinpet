'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Notifications', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      isOpen: {
        type: Sequelize.BOOLEAN
      },
      type: {
        allowNull: false,
        type: Sequelize.ENUM('comment', 'like', 'request')
      },
      isRequest: {
        type: Sequelize.ENUM('Netral', 'Approved', 'Rejected')
      },
      PetId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      SenderId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      ReceiverId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      RequestId: {
        type: Sequelize.INTEGER
      },
      CommentId: Sequelize.INTEGER,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Notifications');
  }
};