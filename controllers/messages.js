const {
    Messages,
    User,
    Profile,
    MessageCount
} = require('../models');

module.exports = {
    async create(req, res, next) {
        try {
            const {
                message
            } = req.body
            const sendMessage = await Messages.create({
                message,
                UserId: req.dataUser.id,
                SenderId: req.user.id
            });

            res.status(201).json({
                status: 'success',
                data: sendMessage
            })

        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async delete(req, res, next) {
        try {
            const message_id = req.params.id
            await Messages.destroy({
                where: {
                    id: message_id
                }
            })


            res.status(200).json({
                status: 'success',
                message: `Message id: ${message_id} has been deleted`
            })

        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async findAll(req, res, next) {
        try {

            const allMessage = await Messages.findAll({
                include: [{
                    model: User,
                    attributes: {
                        exclude: ['email', 'username', 'password', 'confirm_password', 'createdAt', 'updatedAt']
                    },
                    include: {
                        model: Profile,
                        attributes: ['full_name', 'image_url']
                    }
                }],
                where: {
                    UserId: req.user.id
                },
                order: [
                    ['id', 'DESC']
                ]
            })

            await Messages.update({
                isOpen: true
            }, {
                where: {
                    UserId: req.user.id
                }
            })

            res.status(200).json({
                status: 'success',
                data: allMessage
            })
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async update(req, res, next) {
        try {
            const id = req.params.id
            const {
                message
            } = req.body

            await Messages.update({
                message
            }, {
                where: {
                    id: id
                }
            })

            res.status(202).json({
                status: 'success',
                message: `Data message id: ${id} has been updated`
            })

        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async findAllAndCountAll(req, res, next) {
        try {
            let getMessages = await Messages.findAndCountAll({
                where: {
                    isOpen: false,
                    UserId: req.user.id
                }
            })

            const MessagesCount = getMessages.count

            let newCountMessage = await MessageCount.findOrCreate({
                where: {
                    UserId: req.user.id
                },
                defaults: {
                    messageCount: MessagesCount,
                    UserId: req.user.id
                }
            })

            if (!newCountMessage) {
                await MessageCount.update({
                    messageCount: MessagesCount
                }, {
                    where: {
                        UserId: req.user.id
                    }
                })
            }

            res.status(200).json({
                status: "success",
                data: {
                    UserId: req.user.id,
                    MessagesCount
                }
            })
        } catch (err) {
            res.status(422)
            next(err)
        }
    }
}