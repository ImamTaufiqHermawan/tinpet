const {
    User,
    Profile,
    NotificationCount
} = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {
    async register(req, res, next) {
        const {
            email,
            username,
            password,
            confirm_password
        } = req.body
        try {
            if (password.length < 8 && confirm_password.length < 8) {
                res.status(400).json({
                    status: 'failed',
                    message: 'Password must be at least 8 characters'
                })
            } else if (password !== confirm_password) {
                res.status(400).json({
                    status: 'failed',
                    message: "Password doesn't match"
                })
            }

            const re_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            if (email !== '' && !email.match(re_email)) {
                res.status(400).json({
                    status: 'failed',
                    message: "Format email wrongs"
                })
            }

            const hashedPassword = await bcrypt.hash(password, 10);
            const hashedConfirmPassword = await bcrypt.hash(confirm_password, 10);
            const user = await User.create({
                email: email.toLowerCase(),
                username,
                password: hashedPassword,
                confirm_password: hashedConfirmPassword
            })

            await Profile.create({
                email: user.email,
                full_name: user.username,
                userId: user.id
            })

            await NotificationCount.create({
                UserId: user.id
            })
            res.status(201).json({
                "status": "success",
                data: {
                    id: user.id,
                    email,
                    username
                }
            })
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async login(req, res, next) {
        try {
            let user = await User.findOne({
                where: {
                    email: req.body.email.toLowerCase()
                }
            })

            if (!user) {
                res.status(404).json({
                    status: 'failed',
                    message: "User not found!"
                })
            }

            const token = await jwt.sign({
                id: user.id,
                email: user.email
            }, process.env.JWT_KEY)
            if (user && bcrypt.compareSync(req.body.password, user.password)) {
                res.status(201).json({
                    "status": "success",
                    data: {
                        email: user.email,
                        username: user.username,
                        token
                    }
                })
            } else {
                res.status(400).json({
                    "status": "failed",
                    "message": "Password invalid"
                })
            }
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async userData(req, res, next) {
        try {
            let userData = await User.findByPk(req.user.id, {
                attributes: {
                    exclude: ["updatedAt", "createdAt", "password", "confirm_password"],
                },
                include: {
                    model: Profile,
                    attributes: {
                        exclude: ["updatedAt", "createdAt"],
                    }
                }
            });
            res.status(200).json({
                "status": "success",
                data: {
                    userData
                }
            })
        } catch (err) {
            res.status(422)
            next(err)
        }
    }
}