const {
    Request,
    User,
    Profile,
    Pet,
    Notification
} = require('../models');
const mailgun = require("mailgun-js");
const moment = require('moment')


class Requests {
    static async sendRequest(req, res, next) {
        try {
            const profileData = await Profile.findOne({
                where: {
                    id: req.user.id
                }
            })

            const pet = await Pet.findOne({
                where: {
                    UserId: req.user.id
                }
            })

            if (!pet) {
                res.status(400).json({
                    status: 'failed',
                    message: `You can send request, when you have Posts`
                })
            }

            const {
                date,
                hour,
                message,
                location
            } = req.body

            if (req.user.id === req.post.UserId) {
                res.status(400).json({
                    status: 'failed',
                    message: `You can't request a meet to yourself`
                })
            }

            //! regular expression to match required time format
            let re = /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
            if (hour !== '' && !hour.match(re)) {
                res.status(400).json({
                    status: 'failed',
                    message: "Format time is 'HH:MM'. Invalid time format: " + hour
                })
            }

            let datestring = moment().format('L')
            if (moment(date).format('MM/DD/YYYY') < datestring) {
                res.status(400).json({
                    status: 'failed',
                    message: `Date cannot be before the current day of the month and year`
                })
            }

            const request = await Request.create({
                date,
                hour,
                message,
                location,
                SenderId: req.user.id,
                ProfileId: profileData.id,
                PetId: req.post.id
            })

            const DOMAIN = process.env.DOMAIN_MAILGUN;
            const mg = mailgun({
                apiKey: process.env.APIKEY_MAILGUN,
                domain: DOMAIN
            });
            const data = {
                from: "Tinpet Corp <postmaster@sandbox4b281c7465734e6695aa5ff5b92578dd.mailgun.org>",
                to: req.post.User.email,
                subject: `Someone sent you a request for a meet`,
                html: `<strong>users with this name: ${profileData.full_name} and email: ${profileData.email}, send you a request to meet on the date ${request.date}, time ${request.hour}, location ${request.location} and leave a message containing ${request.message}. Please, check your notification in your account and if you like click a Approved, if you don't like click a Rejected</strong>`
            };

            mg.messages()
                .send(data, function (error, body) {
                    if (body) {
                        console.log(body, 'successfully send email');
                    } else if (!body) {
                        console.log(error.message);
                    }
                });

            const notification = await Notification.create({
                type: 'request',
                PetId: req.post.id,
                ReceiverId: req.post.User.id,
                SenderId: req.user.id,
                RequestId: request.id
            })

            res.status(201).json({
                status: 'success',
                data: {
                    request,
                    notification
                }
            })

        } catch (err) {
            res.status(422)
            next(err)
        }
    }

    static async getAllRequest(req, res, next) {
        try {
            const getAll = await Request.findAll({
                attributes: ['id', 'date', 'hour', 'location', 'isApproved', 'SenderId', 'ProfileId', 'PetId'],
                include: [{
                    model: Profile
                }, {
                    model: User
                }]
            })

            res.status(200).json({
                status: 'success',
                data: getAll
            })
        } catch (err) {
            res.status(422)
            next(err)
        }
    }

    static async delete(req, res, next) {
        try {
            await Request.destroy({
                where: {
                    SenderId: req.user.id
                }
            })

            res.status(200).json({
                status: 'success',
                message: `remove done`
            })
        } catch (err) {
            res.status(422)
            next(err)
        }
    }

    static async approved(req, res, next) {
        try {
            const id = req.params.id
            await Pet.update({
                status: 'Matched'
            }, {
                where: {
                    id: id
                }
            })

            await Request.update({
                isApproved: 'Approved'
            }, {
                where: {
                    PetId: id
                }
            })


            res.status(202).json({
                status: 'success',
                message: `Pet status id: ${id} has been updated status to be matched`
            })

        } catch (err) {
            res.status(404)
            next(err)
        }
    }

    static async rejected(req, res, next) {
        try {
            const id = req.params.id
            await Pet.update({
                status: 'Available'
            }, {
                where: {
                    id: id
                }
            })

            await Request.update({
                isApproved: 'Rejected'
            }, {
                where: {
                    id: id
                }
            })


            res.status(202).json({
                status: 'success',
                message: `Pet status id: ${id} has been updated still available`
            })

        } catch (err) {
            res.status(404)
            next(err)
        }
    }
}

module.exports = Requests