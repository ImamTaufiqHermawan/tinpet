const {
    Search,
    Pet,
    Profile,
    User,
    Like,
    Comment
} = require('../models');
const {
    Op
} = require('sequelize');

class Searchs {
    static async searchByCategoryAndLocation(req, res, next) {
        try {
            let category = req.query.category
            let location = req.query.location
            if (category && location) {
                const search = await Search.findAll({
                    where: {
                        [Op.and]: {
                            category: {
                                [Op.like]: `%${category}%`
                            },
                            location: {
                                [Op.like]: `%${location}%`
                            }
                        }
                    },
                    include: [{
                        model: Pet,
                        include: [{
                            model: Profile,
                            attributes: ['image_url']
                        }, {
                            model: Like,
                            attributes: ['SenderId', 'isLike']
                        }, {
                            model: Comment,
                            attributes: ['id', 'SenderId', 'comment'],
                            include: {
                                model: User,
                                attributes: ['username']
                            },
                            order: [
                                ['id', 'DESC']
                            ]
                        }],
                        order: [
                            ['id', 'DESC']
                        ]
                    }],
                    order: [
                        ['id', 'DESC']
                    ]
                })

                res.status(200).json({
                    status: 'success',
                    data: search
                })
            } else {
                const search = await Search.findAll({
                    include: [{
                        model: Pet,
                        include: [{
                            model: Profile,
                            attributes: ['image_url']
                        }, {
                            model: Like,
                            attributes: ['SenderId', 'isLike']
                        }, {
                            model: Comment,
                            attributes: ['id', 'SenderId', 'comment'],
                            include: {
                                model: User,
                                attributes: ['username']
                            },
                            order: [
                                ['id', 'DESC']
                            ]
                        }],
                        order: [
                            ['id', 'DESC']
                        ]
                    }],
                    order: [
                        ['id', 'DESC']
                    ]
                })


                res.status(200).json({
                    status: 'success',
                    data: search
                })
            }
        } catch (err) {
            res.status(400)
            next(err)
        }
    }
}

module.exports = Searchs