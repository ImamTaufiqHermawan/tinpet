const {
    Pet,
    Comment,
    Notification,
    Profile,
    User
} = require('../models');

module.exports = {
    async comment(req, res, next) {
        try {
            let newComment = await Comment.create({
                PetId: req.params.id,
                comment: req.body.comment,
                SenderId: req.user.id
            })

            if (req.user.id !== req.post.UserId) {
                await Notification.create({
                    type: "comment",
                    PetId: req.params.id,
                    SenderId: req.user.id,
                    ReceiverId: req.post.UserId,
                    CommentId: newComment.id
                })
            }

            let getComments = await Comment.findAndCountAll({
                where: {
                    PetId: req.params.id
                }
            })

            const commentsCount = getComments.count;
            await Pet.update({
                commentCounter: commentsCount
            }, {
                where: {
                    id: req.params.id
                }
            })
            res.status(201).json({
                "status": "success",
                data: {
                    newComment
                }
            })
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async getComment(req, res, next) {
        try {
            let getComments = await Comment.findAndCountAll({
                where: {
                    PetId: req.params.id
                },
                order: [
                    ['id', 'DESC']
                ],
                include: [{
                    model: Pet,
                    include: [{
                        model: Profile,
                        attributes: ['id', 'full_name', 'image_url']
                    }]
                }, {
                    model: User,
                    attributes: ['id', 'email', 'username'],
                    include: [{
                        model: Profile,
                        attributes: ['id', 'full_name', 'image_url']
                    }]
                }]
            })

            const commentsCount = getComments.count;
            await Pet.update({
                commentCounter: commentsCount
            }, {
                where: {
                    id: req.params.id
                }
            })
            res.status(200).json({
                "status": "success",
                "message": `These are all comments for Pet ID ${req.params.id} and have ${commentsCount} comments`,
                data: {
                    commentsCount,
                    getComments
                }
            })
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async deleteComment(req, res, next) {
        try {
            let getComment = await Comment.findOne({
                where: {
                    id: req.params.id
                }
            })
            if (!getComment) {
                res.status(400).json({
                    status: 'failed',
                    message: "no comment on this id"
                })
            }
            const pet_id = getComment.PetId;
            await Comment.destroy({
                where: {
                    id: req.params.id
                }
            })

            await Notification.destroy({
                where: {
                    CommentId: req.params.id
                }
            })

            let getComments = await Comment.findAndCountAll({
                where: {
                    PetId: pet_id
                }
            })

            const commentsCount = getComments.count;
            await Pet.update({
                commentCounter: commentsCount
            }, {
                where: {
                    id: pet_id
                }
            })
            res.status(200).json({
                status: 'success',
                message: `success delete comment`
            })
        } catch (err) {
            res.status(422)
            next(err)
        }
    }
}