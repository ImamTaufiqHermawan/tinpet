const {
    Sequelize,
} = require('sequelize');
const {
    Op
} = Sequelize.Op;
const {
    User,
    Profile,
    Notification,
    NotificationCount,
    Pet
} = require('../models');

module.exports = {
    async getNotifications(req, res, next) {
        try {
            const allNotif = await Notification.findAll({
                where: {
                    ReceiverId: req.user.id
                },
                attributes: ["id", "type", "PetId", "SenderId", "ReceiverId"],
                include: {
                    model: Pet
                },
                order: [
                    ['id', 'DESC']
                ]
            })

            const allSenderId = await allNotif.map(x => x.SenderId);
            let promises = await User.findAll({
                where: {
                    id: {
                        [Sequelize.Op.in]: allSenderId
                    }
                },
                attributes: ["id", "username"],
                include: {
                    model: Profile,
                    attributes: ["full_name", "image_url"]
                }
            })

            const detailNotification = await allNotif.map(detailNotif => ({
                detailNotif,
                detailUser: promises.find(promise => promise.id === detailNotif.SenderId)
            }))

            await Notification.update({
                isOpen: true
            }, {
                where: {
                    ReceiverId: req.user.id
                }
            })

            await NotificationCount.update({
                notifCount: null
            }, {
                where: {
                    UserId: req.user.id
                }
            })
            res.status(200).json({
                "status": "success",
                data: {
                    detailNotification
                }
            })
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async notificationCount(req, res, next) {
        try {
            let getNotif = await Notification.findAndCountAll({
                where: {
                    isOpen: false,
                    ReceiverId: req.user.id
                }
            })
            const NotifCount = getNotif.count;
            let [notif, newNotifCount] = await NotificationCount.findOrCreate({
                where: {
                    UserId: req.user.id
                },
                defaults: {
                    notifCount: NotifCount,
                    UserId: req.user.id
                }
            })

            if (newNotifCount == false) {
                await NotificationCount.update({
                    notifCount: NotifCount
                }, {
                    where: {
                        UserId: req.user.id
                    }
                })
            }
            res.status(200).json({
                "status": "success",
                data: {
                    UserId: req.user.id,
                    NotifCount
                }
            })
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async delete(req, res, next) {
        try {
            let {
                id
            } = req.params

            await Notification.destroy({
                where: {
                    id
                }
            })

            res.status(200).json({
                status: "success",
                message: `Notification id: ${id} has been deleted`
            })

        } catch (err) {
            res.status(422)
            next(err)
        }
    }
}