const {
    User,
    Profile
} = require('../models');
const imagekit = require('../lib/imagekit');

module.exports = {
    async updateProfile(req, res, next) {
        const full_name = req.body.full_name;
        const email = req.body.email;
        const mobile_number = req.body.mobile_number;
        const full_address = req.body.full_address;
        const description = req.body.description;
        let user_ID = req.user.id;
        const file = req.file;
        if (file) {
            try {
                const validFormat = file.mimetype == 'image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg' || file.mimetype == 'image/gif';
                if (!validFormat) {
                    res.status(400).json({
                        status: 'failed',
                        message: 'Wrong Image Format'
                    })
                }
                const split = await req.file.originalname.split('.');
                const ext = await split[split.length - 1];
                const profilePic = await imagekit.upload({
                    file: req.file.buffer,
                    fileName: `IMG-${Date.now()}.${ext}`
                })
                await User.update({
                        email
                    }, {
                        where: {
                            id: user_ID
                        }
                    }),
                    await Profile.update({
                        full_name,
                        email,
                        image_url: profilePic.url,
                        mobile_number,
                        full_address,
                        description
                    }, {
                        where: {
                            userId: user_ID
                        }
                    })
                res.status(200).json({
                    "status": "success",
                    data: {
                        user_ID,
                        full_name,
                        email,
                        url: profilePic.url,
                        mobile_number,
                        full_address,
                        description
                    }
                })
            } catch (err) {
                res.status(422)
                next(err)
            }
        } else {
            try {
                await User.update({
                        email
                    }, {
                        where: {
                            id: user_ID
                        }
                    }),
                    await Profile.update({
                        full_name,
                        email,
                        mobile_number,
                        full_address,
                        description
                    }, {
                        where: {
                            userId: user_ID
                        }
                    })
                res.status(200).json({
                    "status": "success",
                    data: {
                        user_ID,
                        full_name,
                        email,
                        mobile_number,
                        full_address,
                        description
                    }
                })
            } catch (err) {
                res.status(422)
                next(err)
            }
        }
    }
}