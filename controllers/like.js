const {
    Pet,
    Like,
    Notification
} = require('../models');

module.exports = {
    async like(req, res, next) {
        try {
            let [getLike, newLike] = await Like.findOrCreate({
                where: {
                    PetId: req.params.id,
                    SenderId: req.user.id
                },
                defaults: {
                    isLike: true,
                    SenderId: req.user.id,
                    PetId: req.params.id
                }
            })

            if (req.user.id !== req.post.UserId) {
                await Notification.findOrCreate({
                    where: {
                        type: "like",
                        PetId: req.params.id,
                        SenderId: req.user.id
                    },
                    defaults: {
                        type: "like",
                        PetId: req.params.id,
                        SenderId: req.user.id,
                        ReceiverId: req.post.UserId
                    }
                })
            }

            if (newLike == false && getLike.isLike == true) {
                await Like.update({
                    isLike: false,
                }, {
                    where: {
                        PetId: req.params.id,
                        SenderId: req.user.id
                    }
                })

                if (req.user.id !== req.post.UserId) {
                    await Notification.destroy({
                        where: {
                            type: "like",
                            PetId: req.params.id,
                            SenderId: req.user.id
                        }
                    })
                }

            } else if (newLike == false && getLike.isLike == false) {
                await Like.update({
                    isLike: true,
                }, {
                    where: {
                        PetId: req.params.id,
                        SenderId: req.user.id
                    }
                })

            }

            let userLike = await Like.findOne({
                where: {
                    PetId: req.params.id,
                    SenderId: req.user.id
                }
            })

            let getLikes = await Like.findAndCountAll({
                where: {
                    isLike: true,
                    PetId: req.params.id
                }
            })

            const likesCount = getLikes.count;
            await Pet.update({
                likeCounter: likesCount
            }, {
                where: {
                    id: req.params.id
                }
            })
            res.status(200).json({
                "status": "success",
                data: {
                    likesCount,
                    userLike
                }
            })
        } catch (err) {
            res.status(422)
            next(err)
        }
    }
}