const {
    User,
    Pet,
    Search,
    Profile,
    Like,
    Comment
} = require('../models');
const Imagekit = require('../lib/imagekit');

class Pets {
    static async create(req, res, next) {
        try {
            const morePost = await Pet.findAndCountAll({
                where: {
                    UserId: req.user.id
                }
            })

            if (morePost.count >= 5) {
                res.status(400).json({
                    status: 'failed',
                    message: `You can only post five of your pets, if you want to post more of your pets. Please subscribe to our application`
                })
            }

            const validFormat = req.file.mimetype === 'image/png' || req.file.mimetype === 'image/jpg' || req.file.mimetype === 'image/jpeg' || req.file.mimetype === 'image/gif';
            if (!validFormat) {
                if (!validFormat) {
                    res.status(400).json({
                        status: 'failed',
                        message: 'Wrong Image Format'
                    })
                }
            }
            const split = req.file.originalname.split('.')
            const ext = split[split.length - 1]
            let img = await Imagekit.upload({
                file: req.file.buffer, //required
                fileName: `IMG-${Date.now()}.${ext}`, //required
            })
            const {
                name,
                age,
                gender,
                breed,
                description,
                location,
                category
            } = req.body

            const profile = await Profile.findOne({
                where: {
                    userId: req.user.id
                }
            })

            const pet = await Pet.create({
                name,
                image_url: img.url,
                age,
                gender,
                breed,
                description,
                UserId: req.user.id,
                ProfileId: profile.id
            })

            let petId = pet.id
            const search = await Search.create({
                location,
                category,
                PetId: petId
            })


            res.status(201).json({
                status: 'success',
                data: {
                    pet,
                    search
                }
            })

        } catch (err) {
            res.status(400)
            next(err)
        }
    }

    static async getAll(req, res, next) {
        try {
            const all = await Pet.findAll({
                include: [{
                    model: Search
                }, {
                    model: Profile
                }, {
                    model: Like,
                    attributes: ['SenderId', 'isLike']
                }, {
                    model: Comment,
                    attributes: ['id', 'SenderId', 'comment'],
                    include: {
                        model: User,
                        attributes: ['username']
                    },
                    order: [
                        ['id', 'DESC']
                    ]
                }]
            })

            all.sort((a, b) => b.id - a.id);

            res.status(200).json({
                status: 'success',
                data: all
            })
        } catch (err) {
            res.status(422)
            next(err)
        }
    }

    static async getMyPost(req, res, next) {
        const profile = await Profile.findOne({
            where: {
                userId: req.user.id
            }
        })
        try {
            const myPost = await Pet.findAll({
                where: {
                    ProfileId: profile.id
                },
                include: [{
                    model: Search
                }, {
                    model: Profile
                }, {
                    model: Like,
                    attributes: ['SenderId', 'isLike']
                }, {
                    model: Comment,
                    attributes: ['id', 'SenderId', 'comment'],
                    include: {
                        model: User,
                        attributes: ['username']
                    },
                    order: [
                        ['id', 'DESC']
                    ]
                }],
                order: [
                    ['id', 'DESC']
                ]
            })

            res.status(200).json({
                status: 'success',
                data: myPost
            })

        } catch (err) {
            res.status(422)
            next(err)
        }
    }

    static async update(req, res, next) {
        try {
            if (req.file) {
                const validFormat = req.file.mimetype === 'image/png' || req.file.mimetype === 'image/jpg' || req.file.mimetype === 'image/jpeg' || req.file.mimetype === 'image/gif';
                if (!validFormat) {
                    if (!validFormat) {
                        res.status(400).json({
                            status: 'failed',
                            message: 'Wrong Image Format'
                        })
                    }
                }
                const split = req.file.originalname.split('.')
                const ext = split[split.length - 1]
                let img = await Imagekit.upload({
                    file: req.file.buffer, //required
                    fileName: `IMG-${Date.now()}.${ext}`, //required
                })

                const {
                    name,
                    age,
                    gender,
                    breed,
                    description,
                    location,
                    category
                } = req.body

                await Pet.update({
                    name,
                    image_url: img.url,
                    age,
                    gender,
                    breed,
                    description,
                }, {
                    where: {
                        id: req.params.id
                    }
                })

                await Search.update({
                    location,
                    category
                }, {
                    where: {
                        PetId: req.params.id
                    }
                })

                res.status(202).json({
                    status: 'success',
                    data: `Pet id: ${req.params.id} has been updated`
                })

            } else {
                const {
                    name,
                    age,
                    gender,
                    breed,
                    description,
                    location,
                    category,
                } = req.body
                await Pet.update({
                    name,
                    age,
                    gender,
                    breed,
                    description
                }, {
                    where: {
                        id: req.params.id
                    }
                })

                await Search.update({
                    location,
                    category
                }, {
                    where: {
                        PetId: req.params.id
                    }
                })


                res.status(202).json({
                    status: 'success',
                    data: `Pet id: ${req.params.id} has been updated`
                })

            }
        } catch (err) {
            res.status(422)
            next(err)
        }
    }

    static async delete(req, res, next) {
        try {
            const id = req.params.id
            const remove = await Pet.destroy({
                where: {
                    id: id
                }
            })

            if (remove.id === req.params.PetId) {
                await Search.destroy({
                    where: {
                        id: id
                    }
                })
            }
            res.status(200).json({
                status: 'success',
                data: `Pet with id: ${id} has been deleted`
            })
        } catch (err) {
            res.status(422)
            next(err)
        }
    }
}

module.exports = Pets