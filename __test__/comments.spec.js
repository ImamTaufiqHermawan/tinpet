const request = require('supertest');
const app = require('../app');
const db = require('../models');
const {
    User,
    Profile,
    Comment,
    Pet
} = require('../models');
const jwt = require('jsonwebtoken')
let token;
let token1;
let getToken;

describe('Comments API Collection', () => {
    beforeAll((done) => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Comments", "Pets" RESTART IDENTITY');
        User.create({
            email: 'token@mail.com',
            username: 'token',
            password: '12345678',
            confirm_password: '12345678'
        }).then(User => {
            Profile.create({
                full_name: User.dataValues.username,
                email: User.dataValues.email,
                userId: User.dataValues.id
            }).then(Profile => {
                token = jwt.sign({
                    id: User.dataValues.id,
                    email: User.dataValues.email
                }, process.env.JWT_KEY);
                done();
            })
        })
    })

    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Comments", "Pets" RESTART IDENTITY');
        User.destroy({
            where: {
                email: 'test@mail.com' && 'token@mail.com'
            },
            truncate: true,
            cascade: true
        })
        Pet.destroy({
            where: {
                name: 'ciki'
            },
            truncate: true,
            cascade: true
        })
    })

    describe('POST /api/v1/users/register', () => {
        test('Should successfully create a new user', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test2@mail.com',
                    username: 'test2',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })
        test('Should successfully create a new user', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test@mail.com',
                    username: 'test',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })
    })

    describe('POST /api/v1/users/login', () => {
        test('Should successfully login with registered account', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test2@mail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    token1 = res.body.data.token
                    done();
                })
        })

        test('Should successfully login with registered account', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test@mail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    getToken = res.body.data.token
                    done();
                })
        })
    })

    describe('POST /api/v1/pets', () => {
        test('should successfully create new Post and status code 201', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'ciki',
                    age: 5,
                    gender: 'male',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'London',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });

        test('should successfully create new Post and status code 201', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'cuki',
                    age: 12,
                    gender: 'female',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'London',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
    });

    describe('POST /api/v1/comments/:id', () => {
        test('Successfully comment', done => {
            request(app)
                .post('/api/v1/comments/1')
                .set('authorization', getToken)
                .set('Content-Type', 'application/json')
                .send({
                    comment: 'What a beautiful Cat'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Successfully comment', done => {
            request(app)
                .post('/api/v1/comments/1')
                .set('authorization', token)
                .set('Content-Type', 'application/json')
                .send({
                    comment: 'What a beautiful Cat'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('fail comment because cannot send empty comment', done => {
            request(app)
                .post('/api/v1/comments/1')
                .set('authorization', getToken)
                .set('Content-Type', 'application/json')
                .send({
                    comment: ''
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('failed');
                    done();
                })
        })

        test('fail comment because null value', done => {
            request(app)
                .post('/api/v1/comments/1')
                .set('authorization', getToken)
                .set('Content-Type', 'application/json')
                .send({
                    comment: null
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('failed');
                    done();
                })
        })
        test('fail comment because null value', done => {
            request(app)
                .post('/api/v1/comments/3')
                .set('authorization', getToken)
                .set('Content-Type', 'application/json')
                .send({
                    comment: 'ada nggk posts-nya'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(400);
                    expect(res.body.status).toEqual('failed');
                    done();
                })
        })
    })

    describe('GET /api/v1/comments/:id', () => {
        test('success show all comments from one pet', done => {
            request(app)
                .get('/api/v1/comments/1')
                .set('authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })

        test('success show all comments from one pet', done => {
            request(app)
                .get('/api/v1/comments/10')
                .set('authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(400);
                    expect(res.body.status).toEqual('failed');
                    done();
                })
        })
    })

    describe('DELETE /api/v1/comments/:id', () => {
        test('Successfully delete comment', done => {
            request(app)
                .delete('/api/v1/comments/1')
                .set('authorization', getToken)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })

        test('fail delete comment because comment does not exist', done => {
            request(app)
                .delete('/api/v1/comments/100')
                .set('authorization', getToken)
                .then(res => {
                    expect(res.statusCode).toEqual(400);
                    expect(res.body.status).toEqual('failed');
                    done();
                })
        })
    })
})