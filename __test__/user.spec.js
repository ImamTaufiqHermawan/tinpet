const request = require('supertest');
const app = require('../app');
const db = require('../models');
const {
    User,
    Profile
} = require('../models');
const jwt = require('jsonwebtoken')
let token;

describe('User API Collection', () => {
    beforeAll((done) => {
        db.sequelize.query('TRUNCATE "Users", "Profiles" RESTART IDENTITY');
        User.create({
            email: 'token@mail.com',
            username: 'token',
            password: '12345678',
            confirm_password: '12345678'
        }).then(User => {
            Profile.create({
                full_name: User.dataValues.username,
                email: User.dataValues.email,
                userId: User.dataValues.id
            }).then(Profile => {
                token = jwt.sign({
                    id: User.dataValues.id,
                    email: User.dataValues.email
                }, process.env.JWT_KEY);
                done();
            })
        })
    })

    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Profiles" RESTART IDENTITY');
        User.destroy({
            where: {
                email: 'test@mail.com' && 'token@mail.com'
            },
            truncate: true,
            cascade: true
        })
    })

    describe('POST /api/v1/users/register', () => {
        test('Should successfully create a new user', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'TEST@mail.com',
                    username: 'test',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })
        test('Should failed registration because incorrect confirmation password', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test6@mail.com',
                    username: 'test6',
                    password: '12345678',
                    confirm_password: '12581078'
                })
                .then(res => {
                    expect(res.body.status).toEqual('failed')
                    expect(res.statusCode).toEqual(400)
                    done();
                })
        })
        test('Should failed registration because password less than 8 characters', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test5@mail.com',
                    username: 'test5',
                    password: '1234567',
                    confirm_password: '1234567'
                })
                .then(res => {
                    expect(res.body.status).toEqual('failed')
                    expect(res.statusCode).toEqual(400)
                    done();
                })
        })
        test('Should failed registration because format email wrong', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test10mail.com',
                    username: 'test3',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.body.status).toEqual('failed')
                    expect(res.statusCode).toEqual(400)
                    done();
                })
        })
        test('Should failed registration because email already exist', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test@mail.com',
                    username: 'test',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('failed');
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })
        test('Should failed registration because not filled', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: '',
                    username: '',
                    password: '',
                    confirm_password: ''
                })
                .then(res => {
                    expect(res.statusCode).toEqual(400);
                    expect(res.body.status).toEqual('failed');
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })
    })

    describe('POST /api/v1/users/login', () => {
        test('Should successfully login with registered account', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'TEST@MAIL.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    getToken = res.body.data.token
                    done();
                })
        })
        test('Should failed to login with unregistered account', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'unregistered-email@mail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.body.status).toEqual('failed')
                    expect(res.statusCode).toEqual(404)
                    done();
                })
        })
        test('Should failed to login with wrong password', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test@mail.com',
                    password: null
                })
                .then(res => {
                    expect(res.body.status).toEqual('failed')
                    expect(res.statusCode).toEqual(422)
                    done();
                })
        })
        test('Should failed to login with wrong password', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'testmail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.body.status).toEqual('failed')
                    expect(res.statusCode).toEqual(404)
                    done();
                })
        })

        test('Should failed to login with wrong password', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: null,
                    password: '12345678'
                })
                .then(res => {
                    expect(res.body.status).toEqual('failed')
                    expect(res.statusCode).toEqual(422)
                    done();
                })
        })

        test('Should failed to login with wrong password', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test@mail.com',
                    password: 'qwertyuiop'
                })
                .then(res => {
                    expect(res.body.status).toEqual('failed')
                    expect(res.statusCode).toEqual(400)
                    done();
                })
        })
    })

    describe('PUT /api/v1/users/profile', () => {
        test('Success updated profile with image', done => {
            request(app)
                .put('/api/v1/users/profile')
                .set('authorization', token)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    full_name: "test please",
                    email: "test2@mail.com",
                    mobile_number: "022",
                    full_address: "bandung",
                    description: "lalaland"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Success updated profile without image', done => {
            request(app)
                .put('/api/v1/users/profile')
                .set('authorization', token)
                .set('Content-Type', 'application/json')
                .field({
                    full_name: "test please",
                    email: "test2@mail.com",
                    mobile_number: "022",
                    full_address: "bandung",
                    description: "lalaland"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Success updated profile with image', done => {
            request(app)
                .put('/api/v1/users/profile')
                .set('authorization', token)
                .set('Content-Type', 'multipart/form-data')
                .field({
                    full_name: "test please",
                    email: "test2@mail.com",
                    mobile_number: "022",
                    full_address: "bandung",
                    description: "lalaland"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Success updated profile', done => {
            request(app)
                .put('/api/v1/users/profile')
                .set('authorization', token)
                .set('Content-Type', 'multipart/form-data')
                .field({
                    full_name: "test please",
                    email: "test2@mail.com",
                    mobile_number: "022",
                    full_address: "bandung",
                    description: "lalaland"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('failed updated profile because email is invalid', done => {
            request(app)
                .put('/api/v1/users/profile')
                .set('authorization', token)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    full_name: "test please",
                    email: "test4mail.com",
                    mobile_number: "022",
                    full_address: "bandung",
                    description: "lalaland"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('failed');
                    done();
                })
        })
        test('failed updated profile because email and full_name not filled', done => {
            request(app)
                .put('/api/v1/users/profile')
                .set('authorization', token)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    full_name: "",
                    email: "",
                    mobile_number: "022",
                    full_address: "bandung",
                    description: "lalaland"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('failed');
                    done();
                })
        })
        test('failed updated profile because wrong image format', done => {
            request(app)
                .put('/api/v1/users/profile')
                .set('authorization', token)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_example/doc.pdf')
                .field({
                    full_name: "imam",
                    email: "imam@mail.com",
                    mobile_number: "022",
                    full_address: "bandung",
                    description: "lalaland"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(400);
                    expect(res.body.status).toEqual('failed');
                    done();
                })
        })


        test('failed updated profile because have not token', done => {
            request(app)
                .put('/api/v1/users/profile')
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    full_name: "imam",
                    email: "imam@mail.com",
                    mobile_number: "022",
                    full_address: "bandung",
                    description: "lalaland"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('failed');
                    done();
                })
        })
    })

    describe('GET /api/v1/users', () => {
        test('success show user data including profile', done => {
            request(app)
                .get('/api/v1/users')
                .set('authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
})