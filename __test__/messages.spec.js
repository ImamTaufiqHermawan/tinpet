const request = require('supertest');
const app = require('../app');
const db = require('../models');
const {
    User,
    Profile
} = require('../models');
const jwt = require('jsonwebtoken')

let token1;
let token2;

describe('User API Collection', () => {
    beforeAll((done) => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Messages" RESTART IDENTITY');
        User.create({
            email: 'token@mail.com',
            username: 'token',
            password: '12345678',
            confirm_password: '12345678'
        }).then(User => {
            Profile.create({
                full_name: User.dataValues.username,
                email: User.dataValues.email,
                userId: User.dataValues.id
            }).then(Profile => {
                token = jwt.sign({
                    id: User.dataValues.id,
                    email: User.dataValues.email
                }, process.env.JWT_KEY);
                done();
            })
        })
    })

    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Messages" RESTART IDENTITY');
        User.destroy({
            where: {
                email: 'test@mail.com' && 'token@mail.com'
            },
            truncate: true,
            cascade: true
        })
    })


    describe('POST /api/v1/users/register', () => {
        test('Should successfully create a new user', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test@mail.com',
                    username: 'test',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })
        test('Should successfully create a new user', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test1@mail.com',
                    username: 'test1',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })
    })

    describe('POST /api/v1/users/login', () => {
        test('Should successfully login with registered account', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test1@mail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    token1 = res.body.data.token
                    done();
                })
        })
        test('Should successfully login with registered account', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test@mail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    token2 = res.body.data.token
                    done();
                })
        })
    })


    describe('POST /api/v1/messages/:id', () => {
        test('should successfully create messages and status code 201', (done) => {
            request(app)
                .post('/api/v1/messages/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .send({
                    message: 'Hello'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });

        test('should successfully create messages and status code 201', (done) => {
            request(app)
                .post('/api/v1/messages/2')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .send({
                    message: 'Hello'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
        test('should successfully create messages and status code 201', (done) => {
            request(app)
                .post('/api/v1/messages/3')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .send({
                    message: 'Hello'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });

        test('should can not create messages and status code 422', (done) => {
            request(app)
                .post('/api/v1/messages/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .send({
                    message: ''
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });

        test('should can not create messages and status code 422', (done) => {
            request(app)
                .post('/api/v1/messages/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .send({
                    message: null
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });

        test('should can not create messages and status code 422', (done) => {
            request(app)
                .post('/api/v1/messages/1')
                .set('Content-Type', 'application/json')
                .send({
                    message: 'Hello world'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });

        test('should can not create messages and status code 400', (done) => {
            request(app)
                .post('/api/v1/messages/10')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .send({
                    message: 'Hello world'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(400)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
    });


    describe('GET /api/v1/messages/all', () => {
        test('should successfully get all data messages and status code 200', (done) => {
            request(app)
                .get('/api/v1/messages/all')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .then((res) => {
                    expect(res.statusCode).toBe(200)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });

        test('should successfully get all data messages and status code 200', (done) => {
            request(app)
                .get('/api/v1/messages/all')
                .set('Content-Type', 'application/json')
                .set('Authorization', token2)
                .then((res) => {
                    expect(res.statusCode).toBe(200)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
    });

    describe('PUT /api/v1/messages/:id', () => {
        test('should successfully update data message and status code 202', (done) => {
            request(app)
                .put('/api/v1/messages/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .send({
                    message: 'Hi!!'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(202)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });

        test('should can not update data message and status code 400', (done) => {
            request(app)
                .put('/api/v1/messages/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token2)
                .send({
                    message: 'Hello'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(400)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
        test('should can not update data message and status code 400', (done) => {
            request(app)
                .put('/api/v1/messages/10')
                .set('Content-Type', 'application/json')
                .set('Authorization', token2)
                .send({
                    message: 'Hello'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(400)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
        test('should can not update data message and status code 422', (done) => {
            request(app)
                .put('/api/v1/messages/1')
                .set('Content-Type', 'application/json')
                .send({
                    message: 'Hello'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });

        test('should can not update data message and status code 422', (done) => {
            request(app)
                .put('/api/v1/messages/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .send({
                    message: ''
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });

        test('should can not update data message and status code 422', (done) => {
            request(app)
                .put('/api/v1/messages/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .send({
                    message: null
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });


    });

    describe('DELETE /api/v1/messages/:id', () => {
        test('should successfully delete data messages and status code 200', (done) => {
            request(app)
                .delete('/api/v1/messages/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .then((res) => {
                    expect(res.statusCode).toBe(200)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });

        test('should successfully delete data messages and status code 422', (done) => {
            request(app)
                .delete('/api/v1/messages/1')
                .set('Content-Type', 'application/json')
                .then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
    });

    describe('GET /api/v1/messages/count', () => {
        test('should successfully get data messages and count messages, status code 200', (done) => {
            request(app)
                .get('/api/v1/messages/count')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .then((res) => {
                    expect(res.statusCode).toBe(200)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
    });
})