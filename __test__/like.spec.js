const request = require('supertest');
const app = require('../app');
const db = require('../models');
const {
    User,
    Profile,
    Like,
    Pet
} = require('../models');
const jwt = require('jsonwebtoken')
let token;

describe('Likes API Collection', () => {
    beforeAll((done) => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Comments", "Pets" RESTART IDENTITY');
        User.create({
            email: 'token@mail.com',
            username: 'token',
            password: '12345678',
            confirm_password: '12345678'
        }).then(User => {
            Profile.create({
                full_name: User.dataValues.username,
                email: User.dataValues.email,
                userId: User.dataValues.id
            }).then(Profile => {
                token = jwt.sign({
                    id: User.dataValues.id,
                    email: User.dataValues.email
                }, process.env.JWT_KEY);
                done();
            })
        })
    })

    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Comments", "Pets" RESTART IDENTITY');
        User.destroy({
            where: {
                email: 'test@mail.com' && 'token@mail.com'
            },
            truncate: true,
            cascade: true
        })
        Pet.destroy({
            where: {
                name: 'ciki'
            },
            truncate: true,
            cascade: true
        })
        Like.destroy({
            where: {
                SenderId: '1'
            },
            truncate: true,
            cascade: true
        })
    })

    describe('POST /api/v1/pets', () => {
        test('should successfully create new Post and status code 201', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'ciki',
                    age: 5,
                    gender: 'male',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'London',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
    });

    describe('POST /api/v1/likes/:id', () => {
        test('create a new like', done => {
            request(app)
                .post('/api/v1/likes/1')
                .set('authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Successfully unlike', done => {
            request(app)
                .post('/api/v1/likes/1')
                .set('authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Successfully like', done => {
            request(app)
                .post('/api/v1/likes/1')
                .set('authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('fail like because pet does not exist', done => {
            request(app)
                .post('/api/v1/comments/10')
                .set('authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(400);
                    expect(res.body.status).toEqual('failed');
                    done();
                })
        })
        test('fail like because : no token', done => {
            request(app)
                .post('/api/v1/comments/1')
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('failed');
                    done();
                })
        })
    })
})