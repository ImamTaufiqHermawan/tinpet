const request = require('supertest');
const app = require('../app');
const db = require('../models');
const {
    User,
    Profile
} = require('../models');
const jwt = require('jsonwebtoken')

let token;
let token1;
describe('Users, Pets & Searches API Collection', () => {
    beforeAll((done) => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Pets", "Searches" RESTART IDENTITY');
        User.create({
            email: 'token@mail.com',
            username: 'token',
            password: '12345678',
            confirm_password: '12345678'
        }).then(User => {
            Profile.create({
                full_name: User.dataValues.username,
                email: User.dataValues.email,
                userId: User.dataValues.id
            }).then(Profile => {
                token = jwt.sign({
                    id: User.dataValues.id,
                    email: User.dataValues.email
                }, process.env.JWT_KEY);
                done();
            })
        })
    })

    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Pets", "Searches" RESTART IDENTITY');
        User.destroy({
            where: {
                email: 'test@mail.com' && 'token@mail.com'
            },
            truncate: true,
            cascade: true
        })
    })

    describe('POST /api/v1/users/register', () => {
        test('Should successfully create a new user', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test@mail.com',
                    username: 'test',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })

        test('Should successfully create a new user', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'titan@mail.com',
                    username: 'titann',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })
    })

    describe('POST /api/v1/users/login', () => {
        test('Should successfully login with registered account', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test@mail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    token = res.body.data.token
                    done();
                })
        })

        test('Should successfully login with registered account', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'titan@mail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    token1 = res.body.data.token
                    done();
                })
        })
    })

    describe('POST /api/v1/pets', () => {
        test('should successfully create new Post and status code 201', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'aaron',
                    age: 5,
                    gender: 'male',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'London',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
        test('should successfully create new Post and status code 201', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'aaron',
                    age: 5,
                    gender: 'male',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'London',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
        test('should successfully create new Post and status code 201', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'aaron',
                    age: 5,
                    gender: 'male',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'London',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
        test('should successfully create new Post and status code 201', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'aaron',
                    age: 5,
                    gender: 'male',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'London',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
        test('should successfully create new Post and status code 201', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'aaron',
                    age: 5,
                    gender: 'male',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'London',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
        test('should can not create new Post and status code 400', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'aaron',
                    age: 5,
                    gender: 'male',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'London',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(400)
                    expect(res.body.status).toEqual('failed')
                    done()
                })
        });
        test('should can not create new Post and status code 400', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'aaron',
                    age: 5,
                    gender: 'feeeemaleee',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'London',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(400)
                    expect(res.body.status).toEqual('failed')
                    done()
                })
        });
        test('should can not create new Post and status code 400', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'aaron',
                    age: '5 tahun',
                    gender: 'male',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'Jakarta',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(400)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });

        test('should can not create new Post and status code 400', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .field({
                    name: 'aaron',
                    age: 5,
                    gender: 'male',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'jakarta',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(400)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
        test('should failed create new Post because you can create max 5 posts', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .attach('image', './lib/image_example/doc.pdf')
                .field({
                    name: 'aaron',
                    age: 5,
                    gender: 'male',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'London',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(400)
                    expect(res.body.status).toEqual('failed')
                    done()
                })
        });
    });

    describe('GET /api/v1/pets/all', () => {
        test('should successfully get all users Posts and status code 200', (done) => {
            request(app)
                .get('/api/v1/pets/all')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
    });

    describe('GET /api/v1/pets', () => {
        test('should successfully get all My Posts and status code 200', (done) => {
            request(app)
                .get('/api/v1/pets')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
    });

    describe('PUT /api/v1/pets/:id', () => {
        test('should successfully update Posts and status code 202', (done) => {
            request(app)
                .put('/api/v1/pets/1')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'bobo',
                    age: 5,
                    gender: 'Male',
                    breed: 'Persia',
                    description: 'good cat',
                    location: 'Jakarta',
                    category: 'Cat'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(202);
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
        test('should can not update Posts and status code 400', (done) => {
            request(app)
                .put('/api/v1/pets/1')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token1)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'bobo',
                    age: 5,
                    gender: 'Male',
                    breed: 'British',
                    description: 'good cat',
                    location: 'Jakarta',
                    category: 'Cat'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(400);
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
        test('should successfully update Posts and status code 202', (done) => {
            request(app)
                .put('/api/v1/pets/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .send({
                    name: 'bobo',
                    age: 5,
                    gender: 'Male',
                    breed: 'Persia',
                    description: 'good cat',
                    location: 'Jakarta',
                    category: 'Cat'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(202);
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
        test('should can not update Posts and status code 422', (done) => {
            request(app)
                .put('/api/v1/pets/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .send({
                    name: 'bobo',
                    age: 5,
                    gender: 'invalid input value',
                    breed: 'Persia',
                    description: 'good cat',
                    location: 'Jakarta',
                    category: 'Cat'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422);
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
        test('should can not update Posts and status code 422', (done) => {
            request(app)
                .put('/api/v1/pets/1')
                .set('Content-Type', 'application/json')
                .send({
                    name: 'bobo',
                    age: 5,
                    gender: 'Male',
                    breed: 'Persia',
                    description: 'good cat',
                    location: 'Jakarta',
                    category: 'Cat'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422);
                    expect(res.body.status).toEqual('failed')
                    done()
                })
        });
        test('should successfully update Searches and Image, status code 202', (done) => {
            request(app)
                .put('/api/v1/pets/1')
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .set('Authorization', token)
                .field({
                    location: 'Tangerang',
                    category: 'Dogg'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(202);
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
        test('should successfully update Searches and status code 202', (done) => {
            request(app)
                .put('/api/v1/pets/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .send({
                    location: 'Tangerang',
                    category: 'Dogg'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(202);
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
        test('should can not update Posts and status code 422', (done) => {
            request(app)
                .put('/api/v1/pets/1')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'bobo',
                    age: '5 tahun',
                    gender: 'Male',
                    breed: 'Persia',
                    description: 'good cat',
                    location: 'Jakarta',
                    category: 'Cat'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422);
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
        test('should can not update Posts and status code 422', (done) => {
            request(app)
                .put('/api/v1/pets/1')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .attach('image_url', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'bobo',
                    age: '',
                    gender: 'Male',
                    breed: 'Persia',
                    description: 'good cat',
                    location: '',
                    category: ''
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422);
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
        test('should can not update Posts and status code 422', (done) => {
            request(app)
                .put('/api/v1/pets/1')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .field({
                    name: '',
                    age: 5,
                    gender: 'Male',
                    breed: 'Persia',
                    description: 'good cat',
                    location: 'Jakarta',
                    category: 'Cat'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(422);
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
        test('cannot update Posts because wrong image format', (done) => {
            request(app)
                .put('/api/v1/pets/1')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .attach('image', './lib/image_example/doc.pdf')
                .field({
                    name: 'bobo',
                    age: 5,
                    gender: 'Male',
                    breed: 'Persia',
                    description: 'good cat',
                    location: 'Jakarta',
                    category: 'Cat'
                })
                .then((res) => {
                    expect(res.statusCode).toBe(400);
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
    });

    describe('DELETE /api/v1/pets/:id', () => {
        test('should successfully delete Posts and status code 200', (done) => {
            request(app)
                .delete('/api/v1/pets/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });

        test('should can not delete Posts and status code 400', (done) => {
            request(app)
                .delete('/api/v1/pets/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .then((res) => {
                    expect(res.statusCode).toBe(400);
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
        test('should can not delete Posts and status code 422', (done) => {
            request(app)
                .delete('/api/v1/pets/1')
                .set('Content-Type', 'application/json')
                .then((res) => {
                    expect(res.statusCode).toBe(422);
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
    });

    describe('GET /api/v1/searches', () => {
        test('should successfully get data search by Category and location and status code 200', (done) => {
            request(app)
                .get('/api/v1/searches?location=London&category=cat')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });

        test('should successfully get data search by Category and location and status code 200', (done) => {
            request(app)
                .get('/api/v1/searches?location=Jakarta&category=cat')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });

        test('should successfully get data search by Category and location and status code 200', (done) => {
            request(app)
                .get('/api/v1/searches?location=Jakarta&category=Dog')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });


        test('should successfully get data search by Category and location and status code 200', (done) => {
            request(app)
                .get('/api/v1/searches?category=cat')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });

        test('should successfully get data search by Category and location and status code 200', (done) => {
            request(app)
                .get('/api/v1/searches?location=London')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });

        test('should successfully get data search by Category and location and status code 200', (done) => {
            request(app)
                .get(`/api/v1/searches?location=''&category=''`)
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
        test('should successfully get data search by Category and location and status code 200', (done) => {
            request(app)
                .get(`/api/v1/searches?location=&category=`)
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200);
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
    });
})