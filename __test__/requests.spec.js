const request = require('supertest');
const app = require('../app');
const db = require('../models');
const {
    User,
    Profile
} = require('../models');
const jwt = require('jsonwebtoken')
let token;
let token1;
let token2;

describe('Users, Pets & Searches API Collection', () => {
    beforeAll((done) => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Pets", "Searches" RESTART IDENTITY');
        User.create({
            email: 'titanioy98@gmail.com',
            username: 'token',
            password: '12345678',
            confirm_password: '12345678'
        }).then(User => {
            Profile.create({
                full_name: User.dataValues.username,
                email: User.dataValues.email,
                userId: User.dataValues.id,
                mobile_number: '6285947085164'
            }).then(Profile => {
                token1 = jwt.sign({
                    id: User.dataValues.id,
                    email: User.dataValues.email
                }, process.env.JWT_KEY);
                done();
            })
        })
    })

    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Pets", "Searches" RESTART IDENTITY');
        User.destroy({
            where: {
                email: 'jacksparrow133333@gmail.com' && 'titanioy98@gmail.com'
            },
            truncate: true,
            cascade: true
        })
    })

    describe('POST /api/v1/users/register', () => {
        test('Should successfully create a new user', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'jacksparrow133333@gmail.com',
                    username: 'test',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).toHaveProperty('data')
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })

        test('Should successfully create a new user', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'imam@gmail.com',
                    username: 'imam',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).toHaveProperty('data')
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })

        test('Should successfully create a new user', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'titanioyudista98@gmail.com',
                    username: 'test1',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).toHaveProperty('data')
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })

        test('Should failed registreation because format email wrong', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'jacksparrow133333gmail.com',
                    username: 'test2',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(400);
                    expect(res.body.status).toEqual('failed');
                    expect(res.body).toHaveProperty('message')
                    done();
                })
        })

        test('Should failed registration because incorrect confirmation password', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test6@mail.com',
                    username: 'test6',
                    password: '12345678',
                    confirm_password: '12581078'
                })
                .then(res => {
                    expect(res.body.status).toEqual('failed')
                    expect(res.statusCode).toEqual(400)
                    done();
                })
        })

        test('Should failed registration because password less than 8 characters', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test5@mail.com',
                    username: 'test5',
                    password: '1234567',
                    confirm_password: '1234567'
                })
                .then(res => {
                    expect(res.body.status).toEqual('failed')
                    expect(res.statusCode).toEqual(400)
                    done();
                })
        })

        test('Should failed registration because email already exist', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'jacksparrow133333@gmail.com',
                    username: 'test',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('failed');
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })
    })

    describe('POST /api/v1/users/login', () => {
        test('Should successfully login with registered account', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'jacksparrow133333@gmail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    token = res.body.data.token
                    done();
                })
        })

        test('Should successfully login with registered account', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'imam@gmail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    token2 = res.body.data.token
                    done();
                })
        })

        test('Should successfully login with registered account', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'titanioyudista98@gmail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    token1 = res.body.data.token
                    done();
                })
        })

        test('Should failed to login with unregistered account', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'unregistered-email@mail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.body.status).toEqual('failed')
                    expect(res.statusCode).toEqual(404)
                    expect(res.body).toHaveProperty('message')
                    done();
                })
        })
        test('Should failed to login with wrong password', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'jacksparrow133333@gmail.com',
                    password: null
                })
                .then(res => {
                    expect(res.body.status).toEqual('failed')
                    expect(res.statusCode).toEqual(422)
                    expect(res.body).toHaveProperty('message')
                    done();
                })
        })

        test('Should failed to login with wrong password', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'jacksparrow133333gmail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.body.status).toEqual('failed')
                    expect(res.statusCode).toEqual(404)
                    expect(res.body).toHaveProperty('message')
                    done();
                })
        })
    })

    describe('PUT /api/v1/users/profile', () => {
        test('Success updated profile', done => {
            request(app)
                .put('/api/v1/users/profile')
                .set('authorization', token)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    full_name: "test please",
                    email: "jacksparrow133333@gmail.com",
                    mobile_number: "628594708516",
                    full_address: "bandung",
                    description: "lalaland"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).toHaveProperty('data')
                    done();
                })
        })

        test('Success updated profile', done => {
            request(app)
                .put('/api/v1/users/profile')
                .set('authorization', token1)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    full_name: "test please",
                    email: "titanioyudista98@gmail.com",
                    mobile_number: "62859470851",
                    full_address: "bandung",
                    description: "lalaland"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).toHaveProperty('data')
                    done();
                })
        })

    })

    describe('POST /api/v1/pets', () => {
        test('should successfully create new Post and status code 201', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'aaron',
                    age: 5,
                    gender: 'male',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'London',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });

        test('should successfully create new Post and status code 201', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'boni',
                    age: 5,
                    gender: 'female',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'Manchaster`',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });

        test('should can not create new Post and status code 422', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'aaron',
                    age: '5 tahun',
                    gender: 'male',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: '',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
    });

    describe('POST /api/v1/requests/:id', () => {
        test('should successfully send notification via email, status code 201', (done) => {
            request(app)
                .post('/api/v1/requests/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .send({
                    date: '08/08/2020',
                    hour: '10:00',
                    message: 'interesting',
                    location: 'Jakarta'
                }).then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
        test('should failed send request because request yourself', (done) => {
            request(app)
                .post('/api/v1/requests/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .send({
                    date: '08/08/2020',
                    hour: '10:00',
                    message: 'interesting',
                    location: 'Jakarta'
                }).then((res) => {
                    expect(res.statusCode).toBe(400)
                    expect(res.body.status).toEqual('failed')
                    done()
                })
        });
        test('should failed send request because user does not have pet/post', (done) => {
            request(app)
                .post('/api/v1/requests/100')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .send({
                    date: '08/08/2020',
                    hour: '10:00',
                    message: 'interesting',
                    location: 'Jakarta'
                }).then((res) => {
                    expect(res.statusCode).toBe(400)
                    expect(res.body.status).toEqual('failed')
                    done()
                })
        });
        test('should failed send request because user does not have pet/post', (done) => {
            request(app)
                .post('/api/v1/requests/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token2)
                .send({
                    date: '08/08/2020',
                    hour: '10:00',
                    message: 'interesting',
                    location: 'Jakarta'
                }).then((res) => {
                    expect(res.statusCode).toBe(400)
                    expect(res.body.status).toEqual('failed')
                    done()
                })
        });
        test('should can not send notification via email, status code 422', (done) => {
            request(app)
                .post('/api/v1/requests/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .send({
                    date: '08/08/2020',
                    hour: '12:10',
                    message: 'interesting',
                    location: ''
                }).then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
        test('should can not send notification via email, status code 422', (done) => {
            request(app)
                .post('/api/v1/requests/1')
                .set('Content-Type', 'application/json')
                .send({
                    date: '08/08/2020',
                    hour: '12:10',
                    message: 'interesting',
                    location: 'Jakarta'
                }).then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
        test('should can not send notification via email, status code 400', (done) => {
            request(app)
                .post('/api/v1/requests/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .send({
                    date: '08/08/2020',
                    hour: '11',
                    message: 'interesting',
                    location: 'Jakarta'
                }).then((res) => {
                    expect(res.statusCode).toBe(400)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
        test('should can not send notification via email, status code 400', (done) => {
            request(app)
                .post('/api/v1/requests/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .send({
                    date: '07/07/2020',
                    hour: '11:20',
                    message: 'interesting',
                    location: 'Jakarta'
                }).then((res) => {
                    expect(res.statusCode).toBe(400)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
        test('should can not send notification via email, status code 422', (done) => {
            request(app)
                .post('/api/v1/requests/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token1)
                .send({
                    date: '08/08/2020',
                    hour: '11:00',
                    message: 'interesting',
                    location: null
                }).then((res) => {
                    expect(res.statusCode).toBe(422)
                    expect(res.body.status).toEqual('failed')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
    });

    describe('GET /api/v1/requests/', () => {
        test('should successfully get All data requests and status code 200', (done) => {
            request(app)
                .get('/api/v1/requests/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
    })

    describe('PUT /api/v1/requests/rejected/:id', () => {
        test('should successfully updated status pet still Available and status code 202', (done) => {
            request(app)
                .put('/api/v1/requests/rejected/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(202)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
    });

    describe('PUT /api/v1/requests/approved/:id', () => {
        test('should successfully updated status pet to be Matched and status code 202', (done) => {
            request(app)
                .put('/api/v1/requests/approved/1')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(202)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('message')
                    done()
                })
        });
    });

    describe('DELETE /api/v1/requests/', () => {
        test('should successfully delete All data requests and status code 200', (done) => {
            request(app)
                .get('/api/v1/requests/')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(200)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });

        test('should can not delete All data requests and status code 404', (done) => {
            request(app)
                .get('/api/v1/requests/100')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .then((res) => {
                    expect(res.statusCode).toBe(404)
                    expect(res.body.status).toEqual(undefined)
                    done()
                })
        });
    })
})