const request = require('supertest');
const app = require('../app');
const db = require('../models');
const {
    User,
    Profile,
    Comment,
    Pet
} = require('../models');
const jwt = require('jsonwebtoken')
let token;

describe('Comments API Collection', () => {
    beforeAll((done) => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Comments", "Pets" RESTART IDENTITY');
        User.create({
            email: 'token@mail.com',
            username: 'token',
            password: '12345678',
            confirm_password: '12345678'
        }).then(User => {
            Profile.create({
                full_name: User.dataValues.username,
                email: User.dataValues.email,
                userId: User.dataValues.id
            }).then(Profile => {
                token3 = jwt.sign({
                    id: User.dataValues.id,
                    email: User.dataValues.email
                }, process.env.JWT_KEY);
                done();
            })
        })
    })

    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Comments", "Pets" RESTART IDENTITY');
        User.destroy({
            where: {
                email: 'token@mail.com'
            },
            truncate: true,
            cascade: true
        })
    })

    describe('POST /api/v1/users/register', () => {
        test('Should successfully create a new user', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test@mail.com',
                    username: 'test',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })

        test('Should successfully create a new user', done => {
            request(app).post('/api/v1/users/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'titan@mail.com',
                    username: 'titann',
                    password: '12345678',
                    confirm_password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })
    })

    describe('POST /api/v1/users/login', () => {
        test('Should successfully login with registered account', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'test@mail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    token = res.body.data.token
                    done();
                })
        })

        test('Should successfully login with registered account', done => {
            request(app).post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'titan@mail.com',
                    password: '12345678'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    token1 = res.body.data.token
                    done();
                })
        })
    })

    describe('POST /api/v1/pets', () => {
        test('should successfully create new Post and status code 201', (done) => {
            request(app)
                .post('/api/v1/pets/')
                .set('Content-Type', 'multipart/form-data')
                .set('Authorization', token)
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: 'aaron',
                    age: 5,
                    gender: 'male',
                    breed: 'british short hair',
                    description: 'good cat',
                    location: 'London',
                    category: 'cat'
                }).then((res) => {
                    expect(res.statusCode).toBe(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body).toHaveProperty('data')
                    done()
                })
        });
    })


    test('Successfully comment', done => {
        request(app)
            .post('/api/v1/comments/1')
            .set('authorization', token1)
            .set('Content-Type', 'application/json')
            .send({
                comment: 'What a beautiful Cat'
            })
            .then(res => {
                expect(res.statusCode).toEqual(201);
                expect(res.body.status).toEqual('success');
                done();
            })
    })

    describe('GET /api/v1/notifications', () => {
        test('success show all notifications from one user', done => {
            request(app)
                .get('/api/v1/notifications')
                .set('authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('GET /api/v1/notifications/count', () => {
        test('successfully get number of notifications for one user', done => {
            request(app)
                .get('/api/v1/notifications/count')
                .set('authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('DELETE /api/v1/notifications/:id', () => {
        test('successfully delete data notification', done => {
            request(app)
                .delete('/api/v1/notifications/1')
                .set('authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
})