'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Comment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Comment.init({
    comment: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Please input your comment'
        }
      }
    },
    SenderId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    PetId: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Comment',
  });
  Comment.associate = function (models) {
    Comment.belongsTo(models.User, {
      foreignKey: 'SenderId'
    })

    Comment.belongsTo(models.Pet, {
      foreignKey: 'PetId'
    })
  };
  return Comment;
};