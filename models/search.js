'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Search extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Search.belongsTo(models.Pet, {
        foreignKey: 'PetId'
      })
    }
  };
  Search.init({
    location: {
      type: DataTypes.TEXT,
      validate: {
        notEmpty: {
          msg: `Input value please`
        }
      }
    },
    category: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Input value please`
        }
      }
    },
    PetId: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Search',
  });
  return Search;
};