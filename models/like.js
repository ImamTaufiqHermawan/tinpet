'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Like extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Like.init({
    isLike: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    SenderId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    PetId: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Like',
  });
  Like.associate = function (models) {
    Like.belongsTo(models.User, {
      foreignKey: 'SenderId'
    })

    Like.belongsTo(models.Pet, {
      foreignKey: 'PetId'
    })
  };
  return Like;
};