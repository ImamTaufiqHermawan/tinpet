'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User.init({
    email: {
      type: DataTypes.STRING,
      unique: {
        args: true,
        msg: 'Email already exist'
      },
      validate: {
        isLowercase: true,
        notEmpty: {
          msg: 'Please input your email'
        },
        isEmail: {
          msg: 'Email is invalid'
        }
      }
    },
    username: {
      type: DataTypes.STRING,
      unique: {
        args: true,
        msg: 'Username already exist'
      },
      validate: {
        notEmpty: {
          msg: 'Please input your username'
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'Please input your password'
        }
      }
    },
    confirm_password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'Please input your confirmation password'
        }
      }
    }
  }, {
    sequelize,
    modelName: 'User',
  });
  User.associate = function (models) {
    User.hasOne(models.Profile, {
      foreignKey: 'userId'
    })

    User.hasOne(models.Pet, {
      foreignKey: 'UserId'
    })

    User.hasMany(models.Request, {
      foreignKey: 'SenderId'
    })

    User.hasMany(models.Like, {
      foreignKey: 'SenderId'
    })

    User.hasMany(models.Comment, {
      foreignKey: 'SenderId'
    })

    User.hasMany(models.Notification, {
      foreignKey: 'ReceiverId'
    })

    User.hasMany(models.Notification, {
      foreignKey: 'SenderId'
    })

    User.hasMany(models.Messages, {
      foreignKey: 'UserId'
    })

    User.hasMany(models.Messages, {
      foreignKey: 'SenderId'
    })

    User.hasOne(models.NotificationCount, {
      foreignKey: 'UserId'
    })

    User.hasOne(models.MessageCount, {
      foreignKey: 'UserId'
    })
  };
  return User;
};