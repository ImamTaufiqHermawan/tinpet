'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Request extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Request.belongsTo(models.Pet, {
        foreignKey: 'PetId'
      })

      Request.belongsTo(models.User, {
        foreignKey: 'SenderId'
      })

      Request.belongsTo(models.Profile, {
        foreignKey: 'ProfileId'
      })
    }
  };
  Request.init({
    date: {
      type: DataTypes.DATEONLY,
      validate: {
        isDate: true,
        notEmpty: {
          msg: `Please input value date 'DD/MM/YYYY'`
        }
      }
    },
    hour: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Pleas input value time format 'HH:MM'`
        }
      }
    },
    message: {
      type: DataTypes.TEXT,
      validate: {
        notEmpty: {
          msg: `input value please!`
        }
      }
    },
    location: {
      type: DataTypes.TEXT,
      validate: {
        notEmpty: {
          msg: `input value please!`
        }
      }
    },
    isApproved: {
      type: DataTypes.ENUM,
      values: ['Netral', 'Approved', 'Rejected'],
      defaultValue: 'Netral',
      validate: {
        notEmpty: {
          msg: `Please input value ['Netral', 'Approved', 'Rejected']!`
        }
      }
    },
    SenderId: DataTypes.INTEGER,
    ProfileId: DataTypes.INTEGER,
    PetId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Request',
  });
  return Request;
};