'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class NotificationCount extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  NotificationCount.init({
    notifCount: DataTypes.INTEGER,
    UserId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'NotificationCount',
  });
  NotificationCount.associate = function (models) {
    NotificationCount.belongsTo(models.User, {
      foreignKey: 'UserId'
    })
  };
  return NotificationCount;
};