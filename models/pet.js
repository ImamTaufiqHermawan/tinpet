'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Pet extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Pet.belongsTo(models.User, {
        foreignKey: 'UserId'
      })

      Pet.hasOne(models.Search, {
        foreignKey: 'PetId'
      })

      Pet.belongsTo(models.Profile, {
        foreignKey: 'ProfileId'
      })

      Pet.hasMany(models.Request, {
        foreignKey: 'PetId'
      })

      Pet.hasMany(models.Like, {
        foreignKey: 'PetId'
      })

      Pet.hasMany(models.Comment, {
        foreignKey: 'PetId'
      })
    }
  };
  Pet.init({
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Please input value`
        }
      }
    },
    image_url: {
      type: DataTypes.TEXT,
      validates: {
        notEmpty: {
          msg: `Please input value`
        }
      }
    },
    age: {
      type: DataTypes.FLOAT,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: `Please input value Float`
        },
        isFloat: true
      }
    },
    gender: {
      type: DataTypes.STRING,
      validate: {
        isIn: {
          args: [
            ['Male', 'Female', 'male', 'female']
          ],
          msg: "Must be Male, Female, male or female"
        },
        notEmpty: {
          msg: `Please input value`
        }
      }
    },
    breed: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Please input value`
        }
      }
    },
    status: {
      type: DataTypes.STRING,
      defaultValue: 'Available',
      validate: {
        isIn: {
          args: [
            ['Available', 'Matched']
          ],
          msg: "Must be Available or Matched"
        },
        notEmpty: {
          msg: `Please input value`
        }
      }
    },
    description: {
      type: DataTypes.TEXT,
      validates: {
        notEmpty: {
          msg: `Please input value`
        }
      }
    },
    commentCounter: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    likeCounter: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    UserId: DataTypes.INTEGER,
    ProfileId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Pet',
  });
  return Pet;
};