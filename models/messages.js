'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Messages extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Messages.belongsTo(models.User, {
        foreignKey: 'UserId'
      })

      Messages.belongsTo(models.User, {
        foreignKey: 'SenderId'
      })
    }
  };
  Messages.init({
    message: {
      type: DataTypes.TEXT,
      validate: {
        notEmpty: {
          msg: `input your message please!`
        }
      }
    },
    isOpen: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false
    },
    UserId: DataTypes.INTEGER,
    SenderId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Messages',
  });
  return Messages;
};