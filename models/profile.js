'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Profile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Profile.init({
    full_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    image_url: {
      type: DataTypes.TEXT,
      defaultValue: 'https://ik.imagekit.io/ckb21lc9cd/default-profile-picture1-744x744_NOYXNCHjw.jpg',
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: true
      }
    },
    mobile_number: {
      type: DataTypes.STRING,
      allowNull: true
    },
    full_address: {
      type: DataTypes.STRING,
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Profile',
  });
  Profile.associate = function (models) {
    // associations can be defined here
    Profile.belongsTo(models.User, {
      foreignKey: 'userId'
    })

    Profile.hasOne(models.Pet, {
      foreignKey: 'ProfileId'
    })

    Profile.hasOne(models.Request, {
      foreignKey: 'ProfileId'
    })
  };
  return Profile;
};