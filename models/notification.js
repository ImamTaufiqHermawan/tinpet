'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Notification extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Notification.init({
    isOpen: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false
    },
    type: {
      type: DataTypes.ENUM,
      values: ['comment', 'like', 'request'],
      validate: {
        notEmpty: true
      }
    },
    isRequest: {
      type: DataTypes.ENUM,
      values: ['Netral', 'Approved', 'Rejected'],
      defaultValue: 'Netral',
      validate: {
        notEmpty: {
          msg: `Please input value 'Netral', 'Approved', or 'Rejected'`
        }
      }
    },
    PetId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    SenderId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    ReceiverId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    CommentId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Notification',
  });

  Notification.associate = function (models) {
    Notification.belongsTo(models.Pet, {
      foreignKey: 'PetId'
    })

    Notification.belongsTo(models.User, {
      foreignKey: 'SenderId'
    })

    Notification.belongsTo(models.User, {
      foreignKey: 'ReceiverId'
    })

    Notification.belongsTo(models.Request, {
      foreignKey: 'RequestId'
    })

    Notification.belongsTo(models.Comment, {
      foreignKey: 'CommentId'
    })
  };
  return Notification;
};