'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MessageCount extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      MessageCount.belongsTo(models.User, {
        foreignKey: 'UserId'
      })
      // define association here
    }
  };
  MessageCount.init({
    messageCount: DataTypes.INTEGER,
    UserId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'MessageCount',
  });
  return MessageCount;
};